# Industrie 4.0 : Séquence Transformer

*Formation Industrie 4.0 / Ingénieur 2000 / Référent Anthony Baillard*

![](https://i.imgur.com/2ekxBRz.png)

*&copy; [Julien Noyer](https://www.linkedin.com/in/julien-n-21219b28/) for [Ingénieurs2000](https://www.ingenieurs2000.com) - All rights reserved for educational purposes only*

---

<br>

# Définition du répertoire

L'objectif de cette séquence est de comprendre l'utilisation de bibliothèques spécialisées en Intelligence artificielle afin de développer des programmes permettant de réaliser de prédictions. Le langage utilisé lors de cette séquence est le Javascript pour aborder les notions de programmation serveur et consolider les connaissance dans ce langage.

- **Supports de cours** https://hackmd.io/@school-inge-2000/HkeX1PPNu
- **Lien vers la version finale** https://brain.dwsapp.io

<br>

## Utilisation du répertoire

BrainJS est une bibliothèque à la fois Frontend et Backend, le projet développé dans ce répertoire met en pratique les notions serveurs en NodeJS, car dans le cadre de l'entrainement d'un réseau de neurones qui peut être gourmand en énergie, il est recommandé de déporté le calcule sur une machine distante, en l'occurence un serveur Web.

Dans la mesure ou nous mettons en place un serveur Web NodeJS, nous devons respecter des donnes pratiques en commençant par ne pas intégrer directement dans un fichier des informations de connexion. Pour ce faire nous utilisons le module *Dotenv* qui à besoin d'un fichier `.env` pour stocker des variables d'environnement. Vous devez donc créer ce fichier à la racine de votre dossier en suivant les information définies dans le fichier `.env.dist` : 

```
# NODE
NODE_PORT=

#BROKET
NODE_BROKER_URL=
NODE_BROKER_PORT=
NODE_BROKER_CLIENT_PORT=
NODE_BROKER_USERNAME=
NODE_BROKER_PASSWORD=
```

<br>

Le fichier `package.json` intègre l'utilisation de *Nodemon* dans le script `start`, vous devez donc l'installer avec la commande suivante avant de poursuivre :

```
sudo npm i -g nodemon
```

<br>

A présent vous pouvez taper les commandes suivantes pour lancer le projet dans votre navigateur :

```
npm i
npm start 
```

---

<br><br><br>

# Notation de la séquence

![](https://i.imgur.com/FC9fnu5.png)

<br>

En plus du formulaire général sur les notions en Data aborder pendant la séquence "**Transformer**", vous aurez à réaliser un **exercice noté sur 20** dans lequel vous devrez mettre en pratique les techniques abordées lors des sessions de cours avec Julien Noyer.

<br>

## Enoncé de l'exercice

Lors des séquences vous aurez travaillé à la fois sur une page Web et sur un serveur NodeJS, vous allez devoir les éditer pour créer une nouvelle page nommée `seattle.ejs` qui reprend la totalité des fonctionnalités développer pour entrainer un réseau de neurones a prédire un type d'Iris.

L'objectif de l'exercice est d'entrainer un réseau de neurones à faire des prédictions météorologiques pour le ville de Seattle. Vous devez télécharger le fichier à l'adresse ci-dessous et adapter le code pour entrainer le réseau de neurone : 

- https://gitlab.com/ingenieurs-2000/3_sequence_transformer/-/blob/master/data/seattle-weather.zip

<br>

Les données ont été combinées pour qu'elles correspondre au modèle nécessaire pour l'entrainement d'un réseau de neurones avec BrainJS. Les objets générés ont les propriétés suivante : 

```json=
{ 
    "input": [0, 6.7, -2.2, 1.4, 0.1], 
    "output": [1, 0, 0, 0, 0], 
    "weather": "drizzle" 
}
```

La dernière valeur du tableau input correspond au mois de l'année divié par 10, cette solution de conversion a été choisie pour simplifier la gestion de l'entrainement.

Avec c'est données, vous devez réaliser les deux demandes suivante


- **Entrainer** : utiliser les données charger pour une prédiction
- **Tester** : réaliser un formulaire pour prédire le "weather"

<br>

## Restitution

Pour restituer votre travail vous devez réaliser un fichier **ZIP** de votre dossier de travail afin que votre correcteur puisse tester le code dans son environnement local.

<br>

Pour créer une archive ZIP sur Mac, il vous suffit de faire un click-droit sur votre dossier de travail et de sélectionner l'option "Compresser...". Pour créer une archive ZIP sur Windows, il vous suffit de faire un click-droit sur votre dossier de travail et de sélectionner l'option "Envoyer vers > Ficher compressé".

- Créer un fichier **ZIP sur Mac** : https://apple.co/30W1cul
- Créer un fichier **ZIP sur Windows** : https://bit.ly/3sgc3vf

<br>

Une fois votre archive créée, il est essentiel que vous respectiez la nomenclature suivante pour nommé votre archive :

- **NOM_PRENOM_JS_TRANSFORMER**

<br>

Vous devez ensuite effectuer votre rendu via le LMS Ingénieur2000 en y déposant votre fichier **ZIP** dans le dossier correspondant à la séquence.

---

<br><br><br>

# Ressources

*Liste de liens utiles pour la mise en place de la séquence*

![](https://i.imgur.com/eAySYs0.png)

- Brain.js https://brain.js.org/#/
- https://blog.fullstacktraining.com/unsupervised-machine-learning-with-javascript/
- ML5.js https://ml5js.org
- Tweet Sentiment Extraction https://www.kaggle.com/c/tweet-sentiment-extraction/
- Neural networks in JavaScript https://scrimba.com/playlist/pVZJQfg
- Setting up a Raspberry Pi headless https://www.raspberrypi.org/documentation/configuration/wireless/headless.md

<br>

---

*&copy; [Julien Noyer](https://www.linkedin.com/in/julien-n-21219b28/) for [Ingénieurs2000](https://www.ingenieurs2000.com) - All rights reserved for educational purposes only*

---