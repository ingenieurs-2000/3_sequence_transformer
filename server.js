/* 
Imports
*/
    require('dotenv').config();
    const express = require('express');
    const mqtt = require('mqtt');
    const Brain = require('brain.js');
    const fs = require('fs');
//

/* 
Serveur configuration
*/
    // Serveur variables
    const server = express();
    const port = process.env.NODE_PORT;

    // Set server view engine
    server.set( 'view engine', 'ejs' );

    // Set static folder
    server.set( 'views', __dirname );
    server.use( express.static(__dirname) );

    // Body parser
    server.use(express.json({limit: '20mb'}));
    server.use(express.urlencoded({ extended: true }));
//

/* 
Set up MQTT
*/
    // Create 
    const broker  = mqtt.connect(`mqtts:${process.env.NODE_BROKER_URL}`, {
        port: process.env.NODE_BROKER_PORT,
        clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
        username: process.env.NODE_BROKER_USERNAME,
        password: process.env.NODE_BROKER_PASSWORD,
    })

    // Function to send MQTT message
    const publishOnTopic = (topic, content, step) => {
        // Check prediction step
        if( step !== 'Prédiction' ){ broker.publish(topic, JSON.stringify({ content, step })) }
        else if( step === 'Prédiction météo' ){ broker.publish(topic, JSON.stringify({ content, step })) }
        else{
            // Set response variables
            let speciesName = null;
            let irisImg = null;

            // Define species
            let stringContent = content.item.toString();

            switch(stringContent){
                case '1,0,0':
                speciesName = 'Iris Setosa';
                irisImg = 'iris-setosa.png';
                break;

                case '0,1,0':
                speciesName = 'Iris Versicolor';
                irisImg = 'iris-versicolor.png';
                break;

                default:
                speciesName = 'Iris Virginica';
                irisImg = 'iris-virginica.png';
                break;
            }

            // Define prediction
            let predictionName = null;
            let predictionImg = null;
            let temp = 0;
            let predictionIndex = 0;
            for( let i = 0; i < content.prediction.length; i++ ){
                if( content.prediction[i] > temp ){ 
                    temp = content.prediction[i];
                    predictionIndex = i
                }
            }

            // Set prediction
            switch(predictionIndex){
                case 0:
                predictionName = 'Iris Setosa';
                predictionImg = 'iris-setosa.png';
                break;

                case 1:
                predictionName = 'Iris Versicolor';
                predictionImg = 'iris-versicolor.png';
                break;

                default:
                predictionName = 'Iris Virginica';
                predictionImg = 'iris-virginica.png';
                break;
            }

            // Send message to the broker
            broker.publish(topic, JSON.stringify({ 
                iris: { name: speciesName, img: irisImg }, 
                prediction: { 
                    name: predictionName, 
                    img: predictionImg,
                    value: [ 
                        content.prediction[0].toFixed(3), 
                        content.prediction[1].toFixed(3), 
                        content.prediction[2].toFixed(3) ] 
                }, 
                isFound: predictionName === speciesName,
                step 
            }))
        }
    }
//

/* 
Set up BrainJS
*/
    // TODO: Create the Neural Network
//

/* 
Set up routes
*/
    // Connection to the Broker
    broker.on('connect', function () {
        // Define home page route
        server.get('/', (req, res) => { res.render('index', {
                data: { 
                    brokerUrl: process.env.NODE_BROKER_URL,
                    brokerClientPort: process.env.NODE_BROKER_CLIENT_PORT,
                    username: process.env.NODE_BROKER_USERNAME,
                    password: process.env.NODE_BROKER_PASSWORD
                }
            }) 
        })

        // Define route to train model
        server.post('/train', (req, res) => {
            // TODO: Train Neural Network

            // Return to client
            return res.json({ msg: 'Train Neural Network', model: undefined });
        })

        // Define route to use trained model
        server.get('/trained', (req, res) => {
            // Send message to the broker
            publishOnTopic('TOPIC_new_message', `Préparation du test`, 'Start' );

            // Importer le modèle
            const trainedModel = require('./net/trained-model-node.js');

            // Tester le réseau de neurones
            require('./data/test.json').forEach(data => {
                // Test data
                const testedData = trainedModel( data.input );

                // Send message to the broker
                publishOnTopic('TOPIC_new_message', `Prédiction: ${testedData}`, 'Test' );
                
                // Send message to the broker
                publishOnTopic( 'TOPIC_new_message', { item: data.output, prediction: testedData }, 'Prédiction' );
            });

            // Send message to the broker
            publishOnTopic('TOPIC_new_message', `Fin du test`, 'End' );

            // Return to client
            return res.json({ msg: 'Neural Network tested'});
        })
    })
    
//



/* 
Launch server
*/
    server.listen(port, () => {
        console.log({
            server: `http://localhost:${port}/`,
            broker: `mqtts:${process.env.NODE_BROKER_URL}:${process.env.NODE_BROKER_PORT}`
        });
    });
//
